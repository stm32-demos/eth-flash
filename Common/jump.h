#ifndef MINIBOOT_COMMON_H
#define MINIBOOT_COMMON_H

#include "stm32h7xx_hal.h"

/* Bootloader jumping stuff */

/*
 * Initializes interrupt service routine (ISR) vectors.
 */
__attribute__((optimize("-O0")))
void jmp_init_vectors(uint32_t *dest, uint32_t *src, unsigned short size);

/*
 * Continues execution of a new application from 'addr'.
 */
__attribute__((optimize("-O0")))
void jmp_goto(uint32_t addr);

#endif // MINIBOOT_COMMON_H
