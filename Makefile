# Common makefile for bootloader *and* the application.
#
# What you need to edit:
#   - the BOOT_SOURCES / APP_SOURCES, the corresponding
#     ..._HEADERS, and possibly ..._ASMSRCS file lists
#   - at your option, the BOOT_NAME / APP_NAME for custom naming,
#   - make your linker scripts match the BOOT_LDSCR / APP_LDSCR
#     variables (either change file names, or set variables
#     appropriately)
#
# You shouldn't have to touch anything else.

LwIP_INCLUDE :=  \
	-I./LwIP/src/include \
	-I./LwIP/src/api \
	-I./LwIP/src/core \
	-I./LwIP/src/netif \
	-I./LwIP/src/apps \
	-I./LwIP/system \
	-DNO_SYS

LwIP_SRC := \
	LwIP/src/api/api_lib.c \
	LwIP/src/api/api_msg.c \
	LwIP/src/api/err.c \
	LwIP/src/api/if_api.c \
	LwIP/src/api/netbuf.c \
	LwIP/src/api/netdb.c \
	LwIP/src/api/netifapi.c \
	LwIP/src/api/sockets.c \
	LwIP/src/api/tcpip.c \
	LwIP/src/core/altcp.c \
	LwIP/src/core/altcp_alloc.c \
	LwIP/src/core/altcp_tcp.c \
	LwIP/src/core/def.c \
	LwIP/src/core/dns.c \
	LwIP/src/core/inet_chksum.c \
	LwIP/src/core/init.c \
	LwIP/src/core/ip.c \
	LwIP/src/core/ipv4/autoip.c \
	LwIP/src/core/ipv4/dhcp.c \
	LwIP/src/core/ipv4/etharp.c \
	LwIP/src/core/ipv4/icmp.c \
	LwIP/src/core/ipv4/igmp.c \
	LwIP/src/core/ipv4/ip4.c \
	LwIP/src/core/ipv4/ip4_addr.c \
	LwIP/src/core/ipv4/ip4_frag.c \
	LwIP/src/core/ipv6/dhcp6.c \
	LwIP/src/core/ipv6/ethip6.c \
	LwIP/src/core/ipv6/icmp6.c \
	LwIP/src/core/ipv6/inet6.c \
	LwIP/src/core/ipv6/ip6.c \
	LwIP/src/core/ipv6/ip6_addr.c \
	LwIP/src/core/ipv6/ip6_frag.c \
	LwIP/src/core/ipv6/mld6.c \
	LwIP/src/core/ipv6/nd6.c \
	LwIP/src/core/mem.c \
	LwIP/src/core/memp.c \
	LwIP/src/core/netif.c \
	LwIP/src/core/pbuf.c \
	LwIP/src/core/raw.c \
	LwIP/src/core/stats.c \
	LwIP/src/core/sys.c \
	LwIP/src/core/tcp.c \
	LwIP/src/core/tcp_in.c \
	LwIP/src/core/tcp_out.c \
	LwIP/src/core/timeouts.c \
	LwIP/src/core/udp.c \
	LwIP/system/OS/sys_arch.c
LwIP_HDR := \
	LwIP/system/arch/bpstruct.h \
	LwIP/system/arch/cc.h \
	LwIP/system/arch/cpu.h \
	LwIP/system/arch/epstruct.h \
	LwIP/system/arch/init.h \
	LwIP/system/arch/lib.h \
	LwIP/system/arch/perf.h \
	LwIP/system/arch/sys_arch.h \
	LwIP/src/include/lwip/arch.h \
	LwIP/src/include/lwip/autoip.h \
	LwIP/src/include/lwip/debug.h \
	LwIP/src/include/lwip/def.h \
	LwIP/src/include/lwip/dhcp.h \
	LwIP/src/include/lwip/dhcp6.h \
	LwIP/src/include/lwip/dns.h \
	LwIP/src/include/lwip/err.h \
	LwIP/src/include/lwip/errno.h \
	LwIP/src/include/lwip/etharp.h \
	LwIP/src/include/lwip/ethip6.h \
	LwIP/src/include/lwip/icmp.h \
	LwIP/src/include/lwip/icmp6.h \
	LwIP/src/include/lwip/if_api.h \
	LwIP/src/include/lwip/igmp.h \
	LwIP/src/include/lwip/inet.h \
	LwIP/src/include/lwip/inet_chksum.h \
	LwIP/src/include/lwip/init.h \
	LwIP/src/include/lwip/ip.h \
	LwIP/src/include/lwip/ip4.h \
	LwIP/src/include/lwip/ip4_addr.h \
	LwIP/src/include/lwip/ip4_frag.h \
	LwIP/src/include/lwip/ip6.h \
	LwIP/src/include/lwip/ip6_addr.h \
	LwIP/src/include/lwip/ip6_frag.h \
	LwIP/src/include/lwip/ip6_zone.h \
	LwIP/src/include/lwip/ip_addr.h \
	LwIP/src/include/lwip/mem.h \
	LwIP/src/include/lwip/memp.h \
	LwIP/src/include/lwip/mld6.h \
	LwIP/src/include/lwip/nd6.h \
	LwIP/src/include/lwip/netbuf.h \
	LwIP/src/include/lwip/netdb.h \
	LwIP/src/include/lwip/netif.h \
	LwIP/src/include/lwip/netifapi.h \
	LwIP/src/include/lwip/opt.h \
	LwIP/src/include/lwip/pbuf.h \
	LwIP/src/include/lwip/priv/altcp_priv.h \
	LwIP/src/include/lwip/priv/api_msg.h \
	LwIP/src/include/lwip/priv/mem_priv.h \
	LwIP/src/include/lwip/priv/memp_priv.h \
	LwIP/src/include/lwip/priv/memp_std.h \
	LwIP/src/include/lwip/priv/nd6_priv.h \
	LwIP/src/include/lwip/priv/raw_priv.h \
	LwIP/src/include/lwip/priv/sockets_priv.h \
	LwIP/src/include/lwip/priv/tcp_priv.h \
	LwIP/src/include/lwip/priv/tcpip_priv.h \
	LwIP/src/include/lwip/prot/autoip.h \
	LwIP/src/include/lwip/prot/dhcp.h \
	LwIP/src/include/lwip/prot/dhcp6.h \
	LwIP/src/include/lwip/prot/dns.h \
	LwIP/src/include/lwip/prot/etharp.h \
	LwIP/src/include/lwip/prot/ethernet.h \
	LwIP/src/include/lwip/prot/iana.h \
	LwIP/src/include/lwip/prot/icmp.h \
	LwIP/src/include/lwip/prot/icmp6.h \
	LwIP/src/include/lwip/prot/ieee.h \
	LwIP/src/include/lwip/prot/igmp.h \
	LwIP/src/include/lwip/prot/ip.h \
	LwIP/src/include/lwip/prot/ip4.h \
	LwIP/src/include/lwip/prot/ip6.h \
	LwIP/src/include/lwip/prot/mld6.h \
	LwIP/src/include/lwip/prot/nd6.h \
	LwIP/src/include/lwip/prot/tcp.h \
	LwIP/src/include/lwip/prot/udp.h \
	LwIP/src/include/lwip/raw.h \
	LwIP/src/include/lwip/sio.h \
	LwIP/src/include/lwip/snmp.h \
	LwIP/src/include/lwip/sockets.h \
	LwIP/src/include/lwip/stats.h \
	LwIP/src/include/lwip/sys.h \
	LwIP/src/include/lwip/tcp.h \
	LwIP/src/include/lwip/tcpbase.h \
	LwIP/src/include/lwip/tcpip.h \
	LwIP/src/include/lwip/timeouts.h \
	LwIP/src/include/lwip/udp.h \
	LwIP/src/include/lwip/altcp.h \
	LwIP/src/include/lwip/altcp_tcp.h \
	LwIP/src/include/lwip/altcp_tls.h \
	LwIP/src/include/lwip/api.h \
	LwIP/src/include/compat/posix/arpa/inet.h \
	LwIP/src/include/compat/posix/net/if.h \
	LwIP/src/include/compat/posix/netdb.h \
	LwIP/src/include/compat/posix/sys/socket.h \
	LwIP/src/include/compat/stdc/errno.h \


## Interface(s)
LwIP_SRC += \
	LwIP/src/netif/bridgeif.c \
	LwIP/src/netif/bridgeif_fdb.c \
	LwIP/src/netif/ethernet.c
LwIP_HDR += \
	LwIP/src/include/netif/bridgeif.h \
	LwIP/src/include/netif/bridgeif_opts.h \
	LwIP/src/include/netif/etharp.h \
	LwIP/src/include/netif/ethernet.h \
	LwIP/src/include/netif/ieee802154.h

## A lot more interfaces, probably unnecessary
#LwIP_SRC += \
	LwIP/src/netif/lowpan6.c \
	LwIP/src/netif/lowpan6_ble.c \
	LwIP/src/netif/lowpan6_common.c \
	LwIP/src/netif/ppp/auth.c \
	LwIP/src/netif/ppp/ccp.c \
	LwIP/src/netif/ppp/chap-md5.c \
	LwIP/src/netif/ppp/chap-new.c \
	LwIP/src/netif/ppp/chap_ms.c \
	LwIP/src/netif/ppp/demand.c \
	LwIP/src/netif/ppp/eap.c \
	LwIP/src/netif/ppp/ecp.c \
	LwIP/src/netif/ppp/eui64.c \
	LwIP/src/netif/ppp/fsm.c \
	LwIP/src/netif/ppp/ipcp.c \
	LwIP/src/netif/ppp/ipv6cp.c \
	LwIP/src/netif/ppp/lcp.c \
	LwIP/src/netif/ppp/magic.c \
	LwIP/src/netif/ppp/mppe.c \
	LwIP/src/netif/ppp/multilink.c \
	LwIP/src/netif/ppp/polarssl/arc4.c \
	LwIP/src/netif/ppp/polarssl/des.c \
	LwIP/src/netif/ppp/polarssl/md4.c \
	LwIP/src/netif/ppp/polarssl/md5.c \
	LwIP/src/netif/ppp/polarssl/sha1.c \
	LwIP/src/netif/ppp/ppp.c \
	LwIP/src/netif/ppp/pppapi.c \
	LwIP/src/netif/ppp/pppcrypt.c \
	LwIP/src/netif/ppp/pppoe.c \
	LwIP/src/netif/ppp/pppol2tp.c \
	LwIP/src/netif/ppp/pppos.c \
	LwIP/src/netif/ppp/upap.c \
	LwIP/src/netif/ppp/utils.c \
	LwIP/src/netif/ppp/vj.c \
	LwIP/src/netif/slipif.c \
	LwIP/src/netif/zepif.c
#LwIP_HDR += \
	LwIP/src/include/netif/lowpan6.h \
	LwIP/src/include/netif/lowpan6_ble.h \
	LwIP/src/include/netif/lowpan6_common.h \
	LwIP/src/include/netif/lowpan6_opts.h \
	LwIP/src/include/netif/ppp/ccp.h \
	LwIP/src/include/netif/ppp/chap-md5.h \
	LwIP/src/include/netif/ppp/chap-new.h \
	LwIP/src/include/netif/ppp/chap_ms.h \
	LwIP/src/include/netif/ppp/eap.h \
	LwIP/src/include/netif/ppp/ecp.h \
	LwIP/src/include/netif/ppp/eui64.h \
	LwIP/src/include/netif/ppp/fsm.h \
	LwIP/src/include/netif/ppp/ipcp.h \
	LwIP/src/include/netif/ppp/ipv6cp.h \
	LwIP/src/include/netif/ppp/lcp.h \
	LwIP/src/include/netif/ppp/magic.h \
	LwIP/src/include/netif/ppp/mppe.h \
	LwIP/src/include/netif/ppp/polarssl/arc4.h \
	LwIP/src/include/netif/ppp/polarssl/des.h \
	LwIP/src/include/netif/ppp/polarssl/md4.h \
	LwIP/src/include/netif/ppp/polarssl/md5.h \
	LwIP/src/include/netif/ppp/polarssl/sha1.h \
	LwIP/src/include/netif/ppp/ppp.h \
	LwIP/src/include/netif/ppp/ppp_impl.h \
	LwIP/src/include/netif/ppp/ppp_opts.h \
	LwIP/src/include/netif/ppp/pppapi.h \
	LwIP/src/include/netif/ppp/pppcrypt.h \
	LwIP/src/include/netif/ppp/pppdebug.h \
	LwIP/src/include/netif/ppp/pppoe.h \
	LwIP/src/include/netif/ppp/pppol2tp.h \
	LwIP/src/include/netif/ppp/pppos.h \
	LwIP/src/include/netif/ppp/upap.h \
	LwIP/src/include/netif/ppp/vj.h \
	LwIP/src/include/netif/slipif.h \
	LwIP/src/include/netif/zepif.h

## Applications
#LwIP_SRC += \
	LwIP/src/apps/altcp_tls/altcp_tls_mbedtls.c \
	LwIP/src/apps/altcp_tls/altcp_tls_mbedtls_mem.c \
	LwIP/src/apps/http/altcp_proxyconnect.c \
	LwIP/src/apps/http/fs.c \
	LwIP/src/apps/http/fsdata.c \
	LwIP/src/apps/http/http_client.c \
	LwIP/src/apps/http/httpd.c \
	LwIP/src/apps/http/makefsdata/makefsdata.c \
	LwIP/src/apps/lwiperf/lwiperf.c \
	LwIP/src/apps/mdns/mdns.c \
	LwIP/src/apps/mqtt/mqtt.c \
	LwIP/src/apps/netbiosns/netbiosns.c \
	LwIP/src/apps/smtp/smtp.c \
	LwIP/src/apps/snmp/snmp_asn1.c \
	LwIP/src/apps/snmp/snmp_core.c \
	LwIP/src/apps/snmp/snmp_mib2.c \
	LwIP/src/apps/snmp/snmp_mib2_icmp.c \
	LwIP/src/apps/snmp/snmp_mib2_interfaces.c \
	LwIP/src/apps/snmp/snmp_mib2_ip.c \
	LwIP/src/apps/snmp/snmp_mib2_snmp.c \
	LwIP/src/apps/snmp/snmp_mib2_system.c \
	LwIP/src/apps/snmp/snmp_mib2_tcp.c \
	LwIP/src/apps/snmp/snmp_mib2_udp.c \
	LwIP/src/apps/snmp/snmp_msg.c \
	LwIP/src/apps/snmp/snmp_netconn.c \
	LwIP/src/apps/snmp/snmp_pbuf_stream.c \
	LwIP/src/apps/snmp/snmp_raw.c \
	LwIP/src/apps/snmp/snmp_scalar.c \
	LwIP/src/apps/snmp/snmp_snmpv2_framework.c \
	LwIP/src/apps/snmp/snmp_snmpv2_usm.c \
	LwIP/src/apps/snmp/snmp_table.c \
	LwIP/src/apps/snmp/snmp_threadsync.c \
	LwIP/src/apps/snmp/snmp_traps.c \
	LwIP/src/apps/snmp/snmpv3.c \
	LwIP/src/apps/snmp/snmpv3_mbedtls.c \
	LwIP/src/apps/sntp/sntp.c \
	LwIP/src/apps/tftp/tftp_server.c
#LwIP_HDR += \
	LwIP/src/apps/altcp_tls/altcp_tls_mbedtls_mem.h \
	LwIP/src/apps/altcp_tls/altcp_tls_mbedtls_structs.h \
	LwIP/src/apps/http/fsdata.h \
	LwIP/src/apps/http/httpd_structs.h \
	LwIP/src/apps/http/makefsdata/tinydir.h \
	LwIP/src/apps/snmp/snmp_asn1.h \
	LwIP/src/apps/snmp/snmp_core_priv.h \
	LwIP/src/apps/snmp/snmp_msg.h \
	LwIP/src/apps/snmp/snmp_pbuf_stream.h \
	LwIP/src/apps/snmp/snmpv3_priv.h \
	LwIP/src/include/lwip/apps/altcp_proxyconnect.h \
	LwIP/src/include/lwip/apps/altcp_tls_mbedtls_opts.h \
	LwIP/src/include/lwip/apps/fs.h \
	LwIP/src/include/lwip/apps/http_client.h \
	LwIP/src/include/lwip/apps/httpd.h \
	LwIP/src/include/lwip/apps/httpd_opts.h \
	LwIP/src/include/lwip/apps/lwiperf.h \
	LwIP/src/include/lwip/apps/mdns.h \
	LwIP/src/include/lwip/apps/mdns_opts.h \
	LwIP/src/include/lwip/apps/mdns_priv.h \
	LwIP/src/include/lwip/apps/mqtt.h \
	LwIP/src/include/lwip/apps/mqtt_opts.h \
	LwIP/src/include/lwip/apps/mqtt_priv.h \
	LwIP/src/include/lwip/apps/netbiosns.h \
	LwIP/src/include/lwip/apps/netbiosns_opts.h \
	LwIP/src/include/lwip/apps/smtp.h \
	LwIP/src/include/lwip/apps/smtp_opts.h \
	LwIP/src/include/lwip/apps/snmp.h \
	LwIP/src/include/lwip/apps/snmp_core.h \
	LwIP/src/include/lwip/apps/snmp_mib2.h \
	LwIP/src/include/lwip/apps/snmp_opts.h \
	LwIP/src/include/lwip/apps/snmp_scalar.h \
	LwIP/src/include/lwip/apps/snmp_snmpv2_framework.h \
	LwIP/src/include/lwip/apps/snmp_snmpv2_usm.h \
	LwIP/src/include/lwip/apps/snmp_table.h \
	LwIP/src/include/lwip/apps/snmp_threadsync.h \
	LwIP/src/include/lwip/apps/snmpv3.h \
	LwIP/src/include/lwip/apps/sntp.h \
	LwIP/src/include/lwip/apps/sntp_opts.h \
	LwIP/src/include/lwip/apps/tftp_opts.h \
	LwIP/src/include/lwip/apps/tftp_server.h

COMMON_SRC := \
	./Core/Src/stm32h7xx_it.c \
	./Core/Src/stm32h7xx_hal_msp.c \
	./Core/Src/system_stm32h7xx.c \
	./Core/Src/syscalls.c \
	./Core/Src/sysmem.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cortex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_eth.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_eth_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_flash.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_flash_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_gpio.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_hsem.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_mdma.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_i2c.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_i2c_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_exti.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_tim.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_tim_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pcd.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pcd_ex.c \
	./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_ll_usb.c \
	./Common/jump.c

COMMON_HDR := \
	./Core/Inc/stm32h7xx_it.h \
	./Core/Inc/stm32h7xx_hal_conf.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/Legacy/stm32_hal_legacy.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_cortex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_eth.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_eth_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_rcc.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_rcc_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_flash.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_flash_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_gpio.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_gpio_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_hsem.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_dma.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_dma_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_mdma.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_pwr.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_pwr_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_def.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_i2c.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_i2c_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_exti.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_tim.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_tim_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_uart.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_uart_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_pcd.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_hal_pcd_ex.h \
	./Drivers/STM32H7xx_HAL_Driver/Inc/stm32h7xx_ll_usb.h \
	./Drivers/CMSIS/Device/ST/STM32H7xx/Include/stm32h743xx.h \
	./Drivers/CMSIS/Device/ST/STM32H7xx/Include/stm32h7xx.h \
	./Drivers/CMSIS/Device/ST/STM32H7xx/Include/system_stm32h7xx.h \
	./Drivers/CMSIS/Include/cmsis_armcc.h \
	./Drivers/CMSIS/Include/cmsis_armclang.h \
	./Drivers/CMSIS/Include/cmsis_compiler.h \
	./Drivers/CMSIS/Include/cmsis_gcc.h \
	./Drivers/CMSIS/Include/cmsis_iccarm.h \
	./Drivers/CMSIS/Include/cmsis_version.h  \
	./Drivers/CMSIS/Include/core_armv8mbl.h  \
	./Drivers/CMSIS/Include/core_armv8mml.h  \
	./Drivers/CMSIS/Include/core_cm0.h \
	./Drivers/CMSIS/Include/core_cm0plus.h \
	./Drivers/CMSIS/Include/core_cm1.h \
	./Drivers/CMSIS/Include/core_cm23.h \
	./Drivers/CMSIS/Include/core_cm3.h \
	./Drivers/CMSIS/Include/core_cm33.h \
	./Drivers/CMSIS/Include/core_cm4.h \
	./Drivers/CMSIS/Include/core_cm7.h \
	./Drivers/CMSIS/Include/core_sc000.h \
	./Drivers/CMSIS/Include/core_sc300.h \
	./Drivers/CMSIS/Include/mpu_armv7.h \
	./Drivers/CMSIS/Include/mpu_armv8.h \
	./Drivers/CMSIS/Include/tz_context.h \
	./Common/jump.h

# bootloader setup
BOOT_NAME    := boot
BOOT_SOURCES := $(COMMON_SRC) ./Boot/main.c $(LwIP_SRC)
BOOT_HEADERS := $(COMMON_HDR) $(LwIP_HDR) ./Boot/lwipopts.h
BOOT_ASMSRCS := ./Core/Startup/startup_stm32h743zitx.s
BOOT_LDSCR   := ./Boot/stm32h7_$(BOOT_NAME).ld
BOOT_INCLUDE := -I./Boot $(LwIP_INCLUDE)

# application setup
APP_NAME     := app
APP_SOURCES  := $(COMMON_SRC) ./App/main.c
APP_HEADERS  := $(COMMON_HDR)
APP_ASMSRCS  := ./Core/Startup/startup_stm32h743zitx.s
APP_LDSCR    := ./App/stm32h7_$(APP_NAME).ld
APP_INCLUDE  := -I./App

#
# You shouldn't have to touch anything below this point
#

# Generating useful variables
APP_OBJS     := $(APP_SOURCES:.c=_$(APP_NAME).o) $(APP_ASMSRCS:.s=_$(APP_NAME).O)
APP_DEPS     := $(APP_SOURCES:.c=.d)
BOOT_OBJS    := $(BOOT_SOURCES:.c=_$(BOOT_NAME).o) $(BOOT_ASMSRCS:.s=_$(BOOT_NAME).O)
BOOT_DEPS    := $(BOOT_SOURCES:.c=.d)

# Toolchain setup
CC     := arm-none-eabi-gcc
CXX    := arm-none-eabi-c++
LD     := arm-none-eabi-ld

INCLUDE := \
	-I./Drivers/CMSIS/Include \
	-I./Drivers/CMSIS/Device/ST/STM32H7xx/Include \
	-I./Drivers/STM32H7xx_HAL_Driver/Inc \
	-I./Core/Inc

## Need these when compiling using HAL / STM32-code
INCLUDE += -DSTM32H743xx -DUSE_HAL_DRIVER -DDEBUG

# CPU architectural stuff
CFLAGS_arch := \
	-mcpu=cortex-m7 \
	--specs=nano.specs \
	-mfpu=fpv5-d16 \
	-mfloat-abi=hard \
	-mthumb

# language-agnostic part of CFLAGS
CFLAGS_cmn = -c $(INCLUDE) $(CFLAGS_arch) #$(CFLAGS_dep)

# specific flags only for c code compilations
CFLAGS_cc = \
	-c -MMD -MP -MF"$(<:.c=.d)" -MT"$@" -MT"$(<:.c=.d)" \
	\
	-O0 -W -Wall -Wextra -Wpedantic -Wno-unused-parameter \
	-fstack-usage \
	-ffunction-sections \
	-fdata-sections \
	-std=gnu11

# specific flags only for assembler compilation
CFLAGS_asm = \
	-x assembler-with-cpp \
	-c -MMD -MP -MF"$(<:.s=.d)" -MT"$@" -MT"$(<:.s=.d)"


LDFLAGS = \
	-mcpu=cortex-m7 \
	--specs=nosys.specs \
	-Wl,-Map="$(@:.elf=.map)" \
	-Wl,--gc-sections \
	-static \
	--specs=nano.specs \
	-mfpu=fpv5-d16 \
	-mfloat-abi=hard \
	-mthumb \
	-Wl,--start-group \
	-lc -lm \
	-Wl,--end-group


all: $(BOOT_NAME).bin $(APP_NAME).bin

.SUFFIXES:
.SUFFIXES: .c .cpp .s .elf

%_$(BOOT_NAME).o: %.c
	$(CC) $(CFLAGS_cc) $(CFLAGS_cmn) $(BOOT_INCLUDE) -o $@ $<

%_$(APP_NAME).o: %.c
	$(CC) $(CFLAGS_cc) $(CFLAGS_cmn) $(APP_INCLUDE)  -o $@ $<

%_$(BOOT_NAME).O: %.s
	$(CC) $(CFLAGS_asm) $(CFLAGS_cmn) $(BOOT_INCLUDE) -o $@ $<

%_$(APP_NAME).O: %.s
	$(CC) $(CFLAGS_asm) $(CFLAGS_cmn) $(BOOT_INCLUDE) -o $@ $<

%.bin: %.elf
	arm-none-eabi-size "$<"
	arm-none-eabi-objdump -h -S "$<"  > "$(<:.elf=.list)"
	arm-none-eabi-objcopy  -O binary "$<" "$@"

$(BOOT_NAME).elf: $(BOOT_OBJS) $(BOOT_LDSCR)
	$(CC) -o $@ $(BOOT_OBJS) -T"$(BOOT_LDSCR)" $(LDFLAGS)

$(APP_NAME).elf: $(APP_OBJS) $(APP_LDSCR)
	$(CC) -o $@ $(APP_OBJS) -T"$(APP_LDSCR)" $(LDFLAGS)

clean:
	rm -rf $(BOOT_DEPS) $(BOOT_OBJS) $(BOOT_OBJS:.o=.su)
	rm -rf $(APP_DEPS)  $(APP_OBJS)  $(APP_OBJS:.o=.su)

	rm -rf $(BOOT_NAME) $(BOOT_NAME).map $(BOOT_NAME).elf
	rm -rf $(APP_NAME)  $(APP_NAME).map  $(APP_NAME).elf

	rm -rf $(BOOT_NAME).bin $(BOOT_NAME).list
	rm -rf $(APP_NAME).bin  $(APP_NAME).list

	rm -rf *~ \%*

-include $(BOOT_DEPS)
-include $(APP_DEPS)
